#!/usr/bin/env bash

ln -s $PWD/.bashrc ~/.bashrc
ln -s $PWD/.vimrc ~/.vimrc
ln -s $PWD/.gvimrc ~/.gvimrc

