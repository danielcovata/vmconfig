alias ls='ls -Gh'
alias ll='ls -l'
alias la='ll -a'
alias lk='ls -lSr'
alias lt='ls -ltr'
alias egrep='egrep -I --color=auto'
alias rgrep='grep -r'
alias less='less -R'
alias mkdir='mkdir -p'
alias h='history'
alias j='jobs -l'
alias whicha='type -a'
alias ..='cd ..'
alias gvim='/c/Program\ Files/Vim/vim74/gvim.exe'


alias a='git add'
alias d='git diff -w'
alias c='git commit'
alias s='git status'
alias p='git pull'
alias s='git status -s'
alias r='git pull --rebase'
alias u='git push'
alias dt='git difftool'
alias mt='git mergetool'

function psgrep () { ps axu | grep -v grep | igrep "$@"; }

